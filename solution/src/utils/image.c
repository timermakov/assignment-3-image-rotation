#include "../../include/image.h"

// Принимает аргументы - ширина, высота, массив пикселей, - возвращает структуру
struct image get_image(uint32_t width, uint32_t height, struct pixel *data) {
    return (struct image) { .width = width, .height = height, .data = data };
}

// Выделяет память, исходя из аргументов - ширина и высота, вызывает формирователь структуры
struct image create_image(uint32_t width, uint32_t height) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
    if (pixels == NULL) {
		return (struct image) {  .width = 0, .height = 0, .data = NULL };
	} else {
		return get_image(width, height, pixels); 
	}
}

// Освобождает память, используя принятый указатель на данные
void destroy_image(struct image* image) {
    free(image->data);
}
