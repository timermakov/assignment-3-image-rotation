#include "../include/file_manager.h"
#include "../include/bmp_manager.h"
#include "../include/rotator.h"

// Описание ошибок открытия исходного файла
const char* open_source_image_errors[] = {
    [OPEN_ERROR]="Unable to open source image for read\n"
};

// Описание ошибок чтения исходного файла
const char* read_source_image_errors[] = {
    [READ_INVALID_SIGNATURE] = "Source image has invalid signature\n",
    [READ_INVALID_BITS] = "Source image has invalid bits\n",
    [READ_INVALID_HEADER] = "Source image has invalid header\n"
};

// Описание ошибок закрытия исходного файла
const char* close_source_image_errors[] = {
	[CLOSE_ERROR] = "Unable to close source image\n"
};

// Описание ошибок открытия преобразованного файла для записи
const char* open_transformed_image_errors[] = {
    [OPEN_ERROR] = "Unable to open target image for writing\n"
};

// Описание ошибок записи в преобразованный файл
const char* write_transformed_image_errors[] = {
	[WRITE_DATA_ERROR] = "Unable to write into target image\n"
};

// Описание ошибок закрытия преобразованного файла
const char* close_transformed_image_errors[] = {
    [CLOSE_ERROR] = "Unable to close target image\n"
};


int main(int argc, char **argv) {
	
	// fprintf(stderr, "%d\n", argc);

	// Проверка на то, что пользователь запускает программу в терминале с нужным количеством аргументов
	// ./image-transformer <source-image> <transformed-image>
    if (argc != 3) {
        fprintf(stderr, "Required arguments: <source-image-path> <transformed-image-path>\n");
        return 1;
    }

    FILE *input_file;

	// Попытка открыть файл для чтения
	enum OPEN_STATUS open_source_image_status = open_file(&input_file, argv[1], "r");
    if (open_source_image_status != OPEN_OK) {
        fprintf(stderr, "%s", open_source_image_errors[open_source_image_status]);
        return 2;
    }

    struct image source_image = {0};

	// Попытка прочитать файл
    enum READ_STATUS read_source_image_status = from_bmp(input_file, &source_image);
    if (read_source_image_status != READ_OK) {
        fprintf(stderr, "%s", read_source_image_errors[read_source_image_status]);
		destroy_image(&source_image);
		close_file(input_file);
        return 3;
    }

	// Попытка закрыть файл
	enum CLOSE_STATUS close_source_image_status = close_file(input_file);
    if (close_source_image_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_source_image_errors[close_source_image_status]);
		destroy_image(&source_image);
		//return 4;
    }

    struct image transformed_image = rotate(source_image);

	// Освобождение памяти от ненужного исходного изображения
    destroy_image(&source_image);

    if (!transformed_image.data){
        fprintf(stderr, "Could not allocate memory\n");
		destroy_image(&transformed_image);
        return 5;
    }

    FILE *output_file;

	// Попытка открыть файл для записи
	enum OPEN_STATUS open_transformed_image_status = open_file(&output_file, argv[2], "w");
    if (open_transformed_image_status != OPEN_OK) {
        fprintf(stderr, "%s", open_transformed_image_errors[open_transformed_image_status]);
		destroy_image(&transformed_image);
        return 6;
    }

	// Попытка записать в файл
	enum WRITE_STATUS write_transformed_image_status = to_bmp(output_file, &transformed_image);
    if (write_transformed_image_status != WRITE_OK) {
        fprintf(stderr, "%s", write_transformed_image_errors[write_transformed_image_status]);
		destroy_image(&transformed_image);
		close_file(output_file);
        return 7;
    }

	// Освобождение памяти после записи трансформированного изображения
	destroy_image(&transformed_image);

	// Попытка закрыть файл
	enum CLOSE_STATUS close_transformed_image_status = close_file(output_file);
    if (close_transformed_image_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_transformed_image_errors[close_transformed_image_status]);
		destroy_image(&transformed_image);
		//return 8;
    }

    fprintf(stdout, "Task completed successfully.\n");

    return 0;
}
