#include "../include/rotator.h"

// Принимает в качестве аргумента изображение, поворачивает его и возвращает
struct image rotate(struct image source) {

    if (!source.data) {
        return get_image(source.height, source.width, NULL);
    }

    struct pixel *new_pixels = malloc(sizeof(struct pixel) * source.width * source.height);

    if(new_pixels){
        for (size_t i = 0; i < source.height; i++) {
            for (size_t j = 0; j < source.width; j++) {
                new_pixels[source.height * j + (source.height - 1 - i)] = source.data[i * source.width + j];
            }
        }
    }

    return get_image(source.height, source.width, new_pixels);
}
