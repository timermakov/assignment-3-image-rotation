#ifndef ROTATOR_H
#define ROTATOR_H

#include "image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

// Повернуть изображение, переданное в source
struct image rotate(struct image source);

#endif
