#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <stdio.h>
#include <stdlib.h>

// Перечисление статусов попытки открытия файла
enum OPEN_STATUS {
    OPEN_OK,
    OPEN_ERROR
};

// Перечисление статусов попытки закрытия файла
enum CLOSE_STATUS {
    CLOSE_OK,
    CLOSE_ERROR
};

// Пытается открыть файл
enum OPEN_STATUS open_file(FILE **file, const char *filename, const char *mode);

// Пытается закрыть файл
enum CLOSE_STATUS close_file(FILE *file);

#endif
