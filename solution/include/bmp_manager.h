#ifndef BMP_MANAGER_H
#define BMP_MANAGER_H

#include "image.h"

// Структура, описывающая заголовок BMP-файла для gcc и clang (атрибут packed для упаковки вплотную)
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

// Перечисление статусов попытки чтения файла
enum READ_STATUS {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

// Перечисление статусов попытки записи в файл
enum WRITE_STATUS {
    WRITE_OK,
    WRITE_DATA_ERROR,
	WRITE_EMPTY_DATA_ERROR,
	WRITE_PADDING_ERROR
};

// Пытается прочитать BMP-файл
enum READ_STATUS from_bmp(FILE *in, struct image *image);

// Пытается записать в BMP-файл
enum WRITE_STATUS to_bmp(FILE *out, const struct image *image);

#endif
